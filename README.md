# Johns Agile Security

Here's a collection of notes and tools regarding I.T. Security.

<br/>


## PKI Cert Grep

- [bashrc_101](https://gitlab.com/umi-ch/bashrc/-/blob/main/bashrc_101)

These are a set of **Bash** shell functions, using **OpenSSL** to quickly decode<br/>
**SSL/TLS certificates** in a **PEM** file, or directly from an online HTTPS endpoint.<br/>
The code is part of the **John's Agile Bashrc** collection -- supporting Linux, MacOS, & WSL.<br/>

-> Click these tags to expand & collapse:

<details>
<summary><b>cert_grep</b></summary>
<pre>
$ mkdir -p ~/zGit/
$ cd       ~/zGit/
$ git clone  https://gitlab.com/umi-ch/bashrc.git  bashrc
$ source   ~/zGit/bashrc/bashrc_101*
&nbsp;
$ i_cert_grep
  ...
&nbsp;
$ cat ~/zGit/bashrc/zPKI/cert-example.pem 
    -----BEGIN CERTIFICATE-----
    MIIDxTCCAq2gAwIBAgIQAqxcJmoLQJuPC3nyrkYldzANBgkqhkiG9w0BAQUFADBs
    MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
    d3cuZGlnaWNlcnQuY29tMSswKQYDVQQDEyJEaWdpQ2VydCBIaWdoIEFzc3VyYW5j
    ZSBFViBSb290IENBMB4XDTA2MTExMDAwMDAwMFoXDTMxMTExMDAwMDAwMFowbDEL
    MAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3
    LmRpZ2ljZXJ0LmNvbTErMCkGA1UEAxMiRGlnaUNlcnQgSGlnaCBBc3N1cmFuY2Ug
    RVYgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMbM5XPm
    +9S75S0tMqbf5YE/yc0lSbZxKsPVlDRnogocsF9ppkCxxLeyj9CYpKlBWTrT3JTW
    PNt0OKRKzE0lgvdKpVMSOO7zSW1xkX5jtqumX8OkhPhPYlG++MXs2ziS4wblCJEM
    xChBVfvLWokVfnHoNb9Ncgk9vjo4UFt3MRuNs8ckRZqnrG0AFFoEt7oT61EKmEFB
    Ik5lYYeBQVCmeVyJ3hlKV9Uu5l0cUyx+mM0aBhakaHPQNAQTXKFx01p8VdteZOE3
    hzBWBOURtCmAEvF5OYiiAhF8J2a3iLd48soKqDirCmTCv2ZdlYTBoSUeh10aUAsg
    EsxBu24LUTi4S8sCAwEAAaNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQF
    MAMBAf8wHQYDVR0OBBYEFLE+w2kD+L9HAdSYJhoIAu9jZCvDMB8GA1UdIwQYMBaA
    FLE+w2kD+L9HAdSYJhoIAu9jZCvDMA0GCSqGSIb3DQEBBQUAA4IBAQAcGgaX3Nec
    nzyIZgYIVyHbIUf4KmeqvxgydkAQV8GK83rZEWWONfqe/EW1ntlMMUu4kehDLI6z
    eM7b41N5cdblIZQB2lWHmiRk9opmzN6cN82oNLFpmyPInngiK3BD41VHMWEZ71jF
    hS9OMPagMRYjyOfiZRYzy78aG6A9+MpeizGLYAiJLQwGXFK3xPkKmNEVX58Svnw2
    Yzi9RKR/5CYrCsSXaQ3pjOLAEFe4yHYSkVXySGnYvCoCWw9E1CAx2/S6cCZdkGCe
    vEsXCS+0yx5DaMkHJ8HSXPfqIbloEpw8nL+e/IBcm2PN7EeqJSdnoDfzAIJ9VNep
    +OkuE6N36B9K
    -----END CERTIFICATE-----
&nbsp;
$ cert_grep ~/zGit/bashrc/zPKI/cert-example.pem 
&nbsp;
    0: Certificate:
      Public-Key: (2048 bit)
      Issuer:   C = US, O = DigiCert Inc, OU = www.digicert.com, CN = DigiCert High Assurance EV Root CA
      Subject:  C = US, O = DigiCert Inc, OU = www.digicert.com, CN = DigiCert High Assurance EV Root CA
      Validity:
         Not Before: Nov 10 00:00:00 2006 GMT
         Not After : Nov 10 00:00:00 2031 GMT
      Data:
         Version: 3 (0x2)
         Serial Number: 02:ac:5c:26:6a:0b:40:9b:8f:0b:79:f2:ae:46:25:77
&nbsp;
      X509v3 extensions:
         X509v3 Key Usage: critical
            Certificate Sign
            CRL Sign
            Digital Signature
         X509v3 Basic Constraints: critical
            CA:TRUE
</pre>
</details>


<details>
<summary><b>crl_grep</b></summary>
<pre>
$ i_crl_grep
  ...
&nbsp;
$ cat ~/zGit/bashrc/zPKI/crl-example.pem
    -----BEGIN X509 CRL-----
    MIIBpjCBjwIBATANBgkqhkiG9w0BAQUFADBNMQswCQYDVQQGEwJYWTEmMCQGA1UE
    CgwdUHl0aG9uIFNvZnR3YXJlIEZvdW5kYXRpb24gQ0ExFjAUBgNVBAMMDW91ci1j
    YS1zZXJ2ZXIXDTEzMTEyMTE3MDg0N1oXDTIzMDkzMDE3MDg0N1qgDjAMMAoGA1Ud
    FAQDAgEAMA0GCSqGSIb3DQEBBQUAA4IBAQCNJXC2mVKauEeN3LlQ3ZtM5gkH3ExH
    +i4bmJjtJn497WwvvoIeUdrmVXgJQR93RtV37hZwN0SXMLlNmUZPH4rHhihayw4m
    unCzVj/OhCCY7/TPjKuJ1O/0XhaLBpBVjQN7R/1ujoRKbSia/CD3vcn7Fqxzw7LK
    fSRCKRGTj1CZiuxrphtFchwALXSiFDy9mr2ZKhImcyq1PydfgEzU78APpOkMQsIC
    UNJ/cf3c9emzf+dUtcMEcejQ3mynBo4eIGg1EW42bz4q4hSjzQlKcBV0muw5qXhc
    HOxH2iTFhQ7SrvVuK/dM14rYM4B5mSX3nRC1kNmXpS9j3wJDhuwmjHed
    -----END X509 CRL-----
&nbsp;
$ crl_grep ~/zGit/bashrc/zPKI/crl-example.pem
&nbsp;
    Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: C = XY, O = Python Software Foundation CA, CN = our-ca-server
        Last Update: Nov 21 17:08:47 2013 GMT
        Next Update: Sep 30 17:08:47 2023 GMT
        CRL extensions:
        X509v3 CRL Number:
        0  [0]
    No Revoked Certificates.
&nbsp;
&nbsp;
# - Also supported: Binary DER file format:
$ file ~/zGit/bashrc/zPKI/crl-example2.crl
    .../zGit/bashrc/zPKI/crl-example2.crl: data
&nbsp;
$ crl_grep ~/zGit/bashrc/zPKI/crl-example2.crl | less
&nbsp;
    Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha384WithRSAEncryption
        Issuer: C = US, O = "DigiCert, Inc.", CN = DigiCert Trusted G4 Code Signing RSA4096 SHA384 2021 CA1
        Last Update: Feb  1 09:00:09 2024 GMT
        Next Update: Feb  8 09:00:09 2024 GMT
        CRL extensions:
        X509v3 Authority Key Identifier:
        68:37:E0:EB:B6:3B:F8:5F:11:86:FB:FE:61:7B:08:88:65:F4:4E:42
        X509v3 CRL Number:
        1006  [3EE]
&nbsp;    
    Revoked Certificates:   
        Serial Number: 05EC0DFCB430D94599C493D1FC5B30A2  [05:EC:0D:FC:B4:30:D9:45:99:C4:93:D1:FC:5B:30:A2]
        Revocation Date: May 31 00:00:01 2021 GMT
    ...
</pre>
</details>


<details>
<summary><b>csr_grep</b></summary>
<pre>
$ i_cert_grep_csr
  ...
&nbsp;
$ cat ~/zGit/bashrc/zPKI/csr-example.pem
    -----BEGIN CERTIFICATE REQUEST-----
    MIICZzCCAU8CAQAwIjEgMB4GA1UEAwwXd2ViZ29hdC1zZWN1cmUubG9jYWxNYWMw
    ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCva1gLLu4KvcRXkemd6lhM
    X7xNYaWaDQ0sOBfVy9gwnsx3iXovBH2pzS7t0ktG22UckJlLsrIfXq7agZnUubVR
    rqlG97EOeVoPB35VDYK95XSMQY8vmEv5JXspjcztqZYZHnGJkSUCtom2bdRbqSav
    MSdLM9ZSZEWxANvKMueTOBXj9o6B8qCVCQH81ouI9wCNniZZyj4c4sQ3C4GP3vcZ
    vEHkLQ/8W0X+dEblGbu6qxjU0NU4keE3T28bHj5AVL0VUlR9RxNH1N0Mxw7ua5iF
    6SoJOMFhZy1TpsO1na/FzsGii4Ohfxlm29j48OrHJArWOyv41knplDpxKw3arQB1
    AgMBAAGgADANBgkqhkiG9w0BAQsFAAOCAQEACuPlDazF8IFDWnRhaSvmsH1HobFu
    vtYMHm6RjfVvsNfgW3Mm4lR2vwmVNnWUMzNYqNgUe/9QQyStg1IevfjuSiMP7NX6
    18j9hxOWC/dm+btUzCkQwTTyDDx2Hy3D+9PNgFFWx0fc8vg6qMJum0Pe8F1Zf1/A
    Iq6gWvHzzJDqIcIxJyFi9Nrx1Sc5xeoGXe4oPvrRhoCNa+tOwAfnqW8KS/mOQSIL
    i3suCEqrQKVYVaZdYjWqkmsDpWNkd2k1TC/UVXdJmHtPr90q8UqoiSvm0HkoyTsT
    PnBK6SmFs3LPD3XHuBso9Q8av6/DA7Hiote1/pYGDECexj+FxAYJm5xr5w==
    -----END CERTIFICATE REQUEST-----
&nbsp;
$ csr_grep ~/zGit/bashrc/zPKI/csr-example.pem
&nbsp;
    Certificate Request:
        Data:
            Version: 1 (0x0)
            Subject: CN = webgoat-secure.localMac
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                    Modulus:
                    Exponent: 65537 (0x10001)
            Attributes:
                (none)
                Requested Extensions:
        Signature Algorithm: sha256WithRSAEncryption
        Signature Value:
</pre>
</details>


<details>
<summary><b>ssl_grep</b></summary>
<pre>
$ i_ssl_grep
  ...
&nbsp;
$ ssl_grep https://bad-domain-name.com
    -> bad-domain-name.com
    -$ nc -z -v  -G 5  bad-domain-name.com  443
    nc: getaddrinfo: nodename nor servname provided, or not known
&nbsp;
$ ssl_grep https://example.com
&nbsp;    
    0: Certificate:
      Public-Key: (2048 bit)
      Issuer:   C = US, O = DigiCert Inc, CN = DigiCert Global G2 TLS RSA SHA256 2020 CA1
      Subject:  C = US, ST = California, L = Los Angeles, O = Internet\C2\A0Corporation\C2\A0for\C2\A0Assigned\C2\A0Names\C2\A0and\C2\A0Numbers, CN = www.example.org
      Validity:
         Not Before: Jan 30 00:00:00 2024 GMT
         Not After : Mar  1 23:59:59 2025 GMT
      Data:
         Version: 3 (0x2)
         Serial Number: 07:5b:ce:f3:06:89:c8:ad:df:13:e5:1a:f4:af:e1:87
&nbsp;
      X509v3 extensions:
         X509v3 Subject Alternative Name:
            DNS:example.com
            DNS:example.edu
            DNS:example.net
            DNS:example.org
            DNS:www.example.com
            DNS:www.example.edu
            DNS:www.example.net
            DNS:www.example.org
         X509v3 Key Usage: critical
            Digital Signature
            Key Encipherment
         X509v3 Extended Key Usage:
            TLS Web Client Authentication
            TLS Web Server Authentication
&nbsp;
    1: Certificate:
      Public-Key: (2048 bit)
      Issuer:   C = US, O = DigiCert Inc, OU = www.digicert.com, CN = DigiCert Global Root G2
      Subject:  C = US, O = DigiCert Inc, CN = DigiCert Global G2 TLS RSA SHA256 2020 CA1
      Validity:
         Not Before: Mar 30 00:00:00 2021 GMT
         Not After : Mar 29 23:59:59 2031 GMT
      Data:
         Version: 3 (0x2)
         Serial Number: 0c:f5:bd:06:2b:56:02:f4:7a:b8:50:2c:23:cc:f0:66
&nbsp;
      X509v3 extensions:
         X509v3 Key Usage: critical
            Certificate Sign
            CRL Sign
            Digital Signature
         X509v3 Extended Key Usage:
            TLS Web Client Authentication
            TLS Web Server Authentication
         X509v3 Basic Constraints: critical
            CA:TRUE
            pathlen:0
</pre>
</details>

<br/>

&nbsp; &nbsp; &nbsp;
![CMMI](pix/cert_grep.png "cret_grep"){width=75%}

<br/>


## PKI Cert Grep Pipeline

- [**John's Agile Cert-Grep Pipeline**](https://gitlab.com/umi-ch/cert-grep)

This extends the **cert_grep** facility above, implementing a **Python** script `cert-info.py`<br/>
which can run both from a Bash CLI session, and in a **GitLab CI/CD Pipeline** to analyze<br/>
thousands of website SSL/TLS certificates in a long-running batch job. Take a look.

&nbsp; &nbsp; &nbsp;
![CMMI](pix/cert_grep_pipeline.png "cret_grep_pipeline"){width=75%}

<br/>


## Managed DevSecOps

- [**2020-06.Managed DevSecOps.pdf**](2020-06.Managed_DevSecOps/2022-10-14.Managed%20DevSecOps.pdf)

This is a short proposal, advocating a managed-service approach to **DevSecOps**.<br/>

Instead of a high-automation approach, fully integrating **SAST** and **DAST** tools into a<br/>
software development team, a better approach is offering this as a **managed service**<br/>
by another small team or external provider.<br/>

This service acts as a **gateway** or **facilitator** -- providing automation as a tool,<br/>
rather than an inflexible approval chain -- because good judgement is needed<br/>
regarding priorities, effective risk, and how to handle questionable false positives.

![Shift Left](pix/Pipeline.png "Shift-Left"){width=75%}

<br/>


## The Future of IT Security

- [**2023-05-08.Future of IT Security.pdf**](2023-05.Future_of_IT_Security/2023-05-08.Future%20of%20IT%20Security.pdf)

Here's a semi-humorous presentation about scaling up IT Security good practices into a<br/>
**business offering**, using **CMMI** principles as a metric.<br/>

Businesses often think of IT Security as a **cost-center**, a financial burden.<br/>
But I propose that it can instead be a **profit-center**, enabling **trusted** business results.<br/>
And the value of **trust**, though somewhat intangible, is quite valuable indeed.

![CMMI](pix/CMMI.png "CMMI"){width=75%}

<br/>


## Star Trick: Captain's Log4J

- [**2023-06-14.Star Trick- Captains Log4J.pdf**](2023-06.Star_Trick_Log4J/2023-06-14.Star%20Trick-%20Captains%20Log4J.pdf)

Here's a humorous presentation illustrating the **Log4J** vulnerability, in **Hollywood** style.<br/>
Are you sure that **your** web applications are protected?

&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
![Borg](pix/Borg.png "Borg"){width=35%}

<br/>


## License
Copyright &copy; 2024, Mountain Informatik GmbH<br/>
All rights reserved.<br/>
Permission to share is granted under the terms of the **Server Side Public License**<br/>
https://www.mongodb.com/licensing/server-side-public-license
<br/>

